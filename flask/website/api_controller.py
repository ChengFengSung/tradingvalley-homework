from flask import Blueprint, request, jsonify
from .models.response import Response
import json

api = Blueprint('api', __name__)

@api.route('/hello', methods=['GET'])
def hello():
  if request.method == 'GET':
    return 'Hello', 200

@api.route('/item', methods=['POST'])
def item():
  if request.method == 'POST':
    request_body = json.loads(request.data)

    print(request_body)

    if request_body == None:
      return '', 400
    elif 'itemName' not in request_body:
      resp = Response('request body should content \'itemName\' property.')
      return json.dumps(resp.__dict__), 400
    else:
      resp = Response('success')
      return json.dumps(resp.__dict__), 200