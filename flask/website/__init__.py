from flask import Flask

def create_app():
  app = Flask(__name__)
  register_controllers(app)
  return app

def register_controllers(app: Flask) -> None:
  from .api_controller import api
  app.register_blueprint(api, url_prefix='/api')