# flask api
`main.py`為程式的進入點，透過 `/website/__init__.py` 處裡flask相關設定 ，由 `api_controller.py` 處理路由
1. `{ROOT-URL}/api/hello`: 
    - Request: 
        - Method: `GET`
    - Response: 
        - Status Code: 200
            - Content: Hello

2. `{ROOT-URL}/api/item`:
    - Request: 
        - Method: `POST`
        - Header:
            - Content-Type: application/json
            - Body: 
                - itemName: `(str)` `(non-nullable)`物件名稱
    - Response:
        - properties:
            - result: `(str)`回傳訊息
        - Status Code: 200
            - Content: `{'result': 'success'}`
        - Status Code: 400
            - Content: `{'result': 'request body should content \'itemName\' property.'}`

# singleton
### method_1.py
實體化物件時由`__new__` fun建立實體，先檢查是否已存在此類別的實體  
尚未存在實體 => 建立實例並回傳  
已存在實例 => 直接回傳已建好的實例  
`method_1.py` 中 `singletonA`、`singletonB` 為相同實體，其`id`相同

### method_2.py
有標註 `@singleton` 的類別其實體化物件都存到 `@singleton` decorator 的 `instances` dict中  
實體化帶有 `@singleton` 的物件時，會先透過 `wrapper` 檢查是否已存在實體  
尚未存在實體 => 建立實例並回傳  
已存在實例 => 直接回傳已建好的實例  
