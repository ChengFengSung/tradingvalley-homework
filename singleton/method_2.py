
def singleton(cls):
  instances = {}
  def wrapper(*args, **kwargs):
    if cls not in instances:
      instances[cls] = cls(*args, **kwargs)
    return instances[cls]
  return wrapper

@singleton
class MyClass:
  def sayHello(self, name: str, message: str) -> None:
    print(f'Hi, {name}. Your message: {message}.')

if __name__ == '__main__':
  classA = MyClass()
  classA.sayHello('Jack', 'Hi Jack')
  print(id(classA))
  
  classB = MyClass()
  classB.sayHello('Helen', 'Hello Helen')
  print(id(classB))