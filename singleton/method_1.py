
class Singleton():
  '''此類別在 Process 運作期間只有一個實例'''
  _instance = None
  def __new__(cls, *args, **kwargs):
    '''
    實體化物件時會觸發__new__ fun
    先檢查是否已存在此類別的實體化
    尚未有實體化物件 => 建立實例並回傳；已存在實例 => 直接回傳已建好的實例
    '''
    if cls._instance is None:
      cls._instance = super().__new__(cls)
    return cls._instance
  
  def sayHello(self, name: str, message: str) -> None:
    print(f'Hi, {name}. Your message: {message}.')

if __name__ == '__main__':
  singletonA = Singleton()
  singletonA.sayHello('Jack', 'Hi Jack')
  print(id(singletonA))
  
  singletonB = Singleton()
  singletonB.sayHello('Helen', 'Hello Helen')
  print(id(singletonB))